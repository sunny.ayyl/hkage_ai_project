import gi
import sys
import os
import requests as req

gi.require_version("Gtk", "3.0")
exec("from gi.repository import Gtk")


class GUI(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Mark AI")
        self.listbox = Gtk.ListBox()
        self.add(self.listbox)
        self.box = Gtk.Box()
        self.listbox.add(self.box)
        self.label = Gtk.Label(label="Time you spend on studing: ")
        self.box.pack_start(self.label, True, True, 0)
        self.text_box = Gtk.Entry()
        self.box.pack_start(self.text_box, True, True, 0)
        self.button = Gtk.Button.new_with_label("Predict mark")
        self.button.connect("clicked", self.predict)
        self.listbox.add(self.button)
        self.listbox.show_all()

    def predict(self, button):
        try:
            text = float(self.text_box.get_text())

            sucess_dialog = Gtk.MessageDialog(text="Sucess", message_type=Gtk.MessageType.INFO,
                                              buttons=Gtk.ButtonsType.OK)

            sucess_dialog.format_secondary_text(
                "Your possible result is " + req.get("http://35.238.206.31/" + self.text_box.get_text()).text)
            sucess_dialog.run()
            sucess_dialog.destroy()
        except:

            fail_dialog = Gtk.MessageDialog(text="Fail", message_type=Gtk.MessageType.ERROR,
                                            buttons=Gtk.ButtonsType.OK)
            fail_dialog.format_secondary_text("Your mark must be number")
            fail_dialog.run()
            fail_dialog.destroy()


window = GUI()
window.connect("destroy", Gtk.main_quit)
window.show_all()
Gtk.main()
